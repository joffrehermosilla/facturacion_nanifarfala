package joffre.NanifarfallaRest.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import joffre.NanifarfallaRest.exception.ResourceNotFoundException;
import joffre.NanifarfallaRest.model.VerificationToken;
import joffre.NanifarfallaRest.repository.VerificationTokenRepository;

@RestController
@RequestMapping("/apiVerificationToken")
public class VerificationTokenController {

	private static final Log LOGGER = LogFactory.getLog(VerificationTokenController.class);
	@Autowired
	VerificationTokenRepository verificationTokenRepository;

	@GetMapping("/VerificationTokens")
	public List<VerificationToken> getAllVerificationTokens() {
		LOGGER.info("INFO TRACE");
		LOGGER.warn("WARNING  TRACE");
		LOGGER.error("ERROR TRACE");
		LOGGER.debug("DEBUG  TRACE");
		return verificationTokenRepository.findAll();
	}

	@PostMapping("/AddaVerificationToken")
	public VerificationToken createVerificationToken(@Valid @RequestBody VerificationToken area) {
		LOGGER.info("INFO TRACE");
		LOGGER.warn("WARNING  TRACE");
		LOGGER.error("ERROR TRACE");
		LOGGER.debug("DEBUG  TRACE");
		return verificationTokenRepository.save(area);
	}

	@GetMapping("/VerificationToken/{id}")
	public VerificationToken getAreaById(@PathVariable(value = "id") int areaId) {
		LOGGER.info("INFO  TRACE");
		LOGGER.warn("WARNING  TRACE");
		LOGGER.error("ERROR TRACE");
		LOGGER.debug("DEBUG  TRACE");
		return verificationTokenRepository.findById(areaId).orElseThrow(() -> new ResourceNotFoundException("VerificationToken", "id", areaId));
	}

	@PutMapping("/VerificationTokens/{id}")
	public VerificationToken updateVerificationToken(@PathVariable(value = "id") int areaId, @Valid @RequestBody VerificationToken areaDetails) {

		VerificationToken area = verificationTokenRepository.findById(areaId)
				.orElseThrow(() -> new ResourceNotFoundException("VerificationToken", "id", areaId));
		area.setNombre_area(areaDetails.getNombre_area());
		area.setVendedor(areaDetails.getVendedor());
		area.setVersion(areaDetails.getVersion());
		area.setClaveApi(areaDetails.getClaveApi());

		VerificationToken updatedArea = verificationTokenRepository.save(area);
		LOGGER.info("METHOD: 'updateVerificationToken'--PARAMS: '" + areaDetails + "'");
		LOGGER.warn("WARNING  TRACE");
		LOGGER.error("ERROR TRACE");
		LOGGER.debug("DEBUG  TRACE");
		return updatedArea;
	}

	@DeleteMapping("/area/{id}")
	public ResponseEntity<?> deleteVerificationToken(@PathVariable(value = "id") int areaId) {
		VerificationToken area = verificationTokenRepository.findById(areaId)
				.orElseThrow(() -> new ResourceNotFoundException("VerificationToken", "id", areaId));

		verificationTokenRepository.delete(area);
		LOGGER.info("METHOD: 'deleteArea'--PARAMS: '" + area + "'");
		LOGGER.warn("WARNING  TRACE");
		LOGGER.error("ERROR TRACE");
		LOGGER.debug("DEBUG  TRACE");
		return ResponseEntity.ok().build();
	}
	// http://localhost:8085/nanifarfalla-service/swagger-ui.html
}
